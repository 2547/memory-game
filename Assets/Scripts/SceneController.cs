﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
    public const int gridRows = 4;
    public const int gridCols = 10;
    public const float offsetX = 2f;
    public const float offsetY = 2.5f;

    private MemoryCard _firstRevealed;
    private int _score;
    private MemoryCard _secondRevealed;
    private int _steps;
    [SerializeField] private Sprite[] images;

    [SerializeField] private MemoryCard originalCard;
    [SerializeField] private TextMesh scoreLabel;
    [SerializeField] private TextMesh stepsLabel;

    public bool CanReveal => _secondRevealed == null;

    public void CardRevealed(MemoryCard card) {
        if (_firstRevealed == null) {
            _firstRevealed = card;
        }
        else {
            _secondRevealed = card;
            stepsLabel.text = "Steps: " + ++_steps;
        }

        StartCoroutine(CheckMatch());
    }

    private IEnumerator CheckMatch() {
        if (_firstRevealed.id == _secondRevealed.id) {
            yield return new WaitForSeconds(.5f);
            scoreLabel.text = "Score: " + ++_score;
            _firstRevealed.gameObject.SetActive(false);
            _secondRevealed.gameObject.SetActive(false);
        }
        else {
            yield return new WaitForSeconds(.5f);
        }

        _firstRevealed.Unrevealed();
        _secondRevealed.Unrevealed();

        _firstRevealed = null;
        _secondRevealed = null;
    }

    // Start is called before the first frame update
    private void Start() {
        var scene = SceneManager.GetActiveScene();
        Debug.Log("Active Scene name is: " + scene.name + "\nActive Scene index: " + scene.buildIndex);

        var startPos = originalCard.transform.position;
        var numbers = new int[40];
        for (var k = 0; k < numbers.Length - 1; k = k + 2)
            if (k == 0) {
                numbers[k] = numbers[k + 1] = k;
            }
            else {
                var temp = k / 2;
                numbers[k] = numbers[k + 1] = temp;
            }

        numbers = ShuffleArray(numbers);

        for (var i = 0; i < gridCols; i++)
        for (var j = 0; j < gridRows; j++) {
            MemoryCard card;
            if (i == 0 && j == 0)
                card = originalCard;
            else
                card = Instantiate(originalCard);

            var index = j * gridCols + i;

            var id = numbers[index];
            card.SetCard(id, images[id]);

            var posX = offsetX * i + startPos.x;
            var posY = -(offsetY * j) + startPos.y;
            card.transform.position = new Vector3(posX, posY, startPos.z);
        }
    }

    public void Restart() {
        SceneManager.LoadScene("SampleScene");
    }

    private int[] ShuffleArray(int[] numbers) {
        var newArray = numbers.Clone() as int[];
        for (var i = 0; i < newArray.Length; i++) {
            var tmp = newArray[i];
            var r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }

        return newArray;
    }

    // Update is called once per frame
    private void Update() {
    }
}